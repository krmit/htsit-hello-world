#!/usr/bin/nodejs

require("colors");

var argv = require("yargs")
.usage("Usage: hello [options] <msg>")
.alias("verbose", "v")
.describe("verbose", "Verbose printing.")
.alias("red", "r")
.describe("red", "Text will be red.")
.alias("green", "g")
.describe("green", "Text will be green.")
.alias("msg", "m")
.describe("msg", "Text to be print.")
.nargs("m", 1)
.alias("help", "h")
.help("h")
.argv;

if(argv["verbose"]) {
    console.log("Starting program!");
}

var text = "hello";

if(typeof argv.msg !== "undefined") { 
    text=argv.msg;
}

var print=text;


if(argv["red"]) {
    print=text.red;
}

if(argv["green"]) {
    print=text.green;
}

if(argv["green"] && argv["red"]) {
    print=text.yellow;
}

console.log(print);

if(argv["verbose"]) {
    console.log("Finish!");
}
